import {themes as prismThemes} from 'prism-react-renderer';
import type {Config} from '@docusaurus/types';
import type * as Preset from '@docusaurus/preset-classic';

const config: Config = {
  title: 'FUTR HUB Documentation',
  tagline: 'All you need to know to use the FUTR HUB platform.',
  favicon: 'https://futr-hub.urbantechrepublic.de/wp-content/uploads/2023/11/favicon-150x150.png',

  // Set the production url of your site here
  url: 'https://berlintxl.gitlab.io',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/futr-hub/platform/core-platform/documentation_public/documentation/',
  //baseUrl: '/' // For local development, use '/'
  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'berlintxl', // Usually your GitHub org/user name.
  projectName: 'Documentation', // Usually your repo name.

  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      {
        docs: {
          sidebarPath: './sidebars.ts',
          routeBasePath: '/',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/berlintxl/futr-hub/platform/core-platform/documentation_public/documentation/-/blob/main/',
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      } satisfies Preset.Options,
    ]
  ],

  themeConfig: {
    // Replace with your project's social card
    image: 'https://futr-hub.urbantechrepublic.de/wp-content/uploads/2023/11/FUTR_HUB_RGB.svg',
    navbar: {
      title: 'Home',
      logo: {
        alt: 'Home',
        src: 'https://futr-hub.urbantechrepublic.de/wp-content/uploads/2023/11/FUTR_HUB_RGB.svg',
      },
      items: [
        {
          type: 'docSidebar',
          sidebarId: 'adminSidebar',
          position: 'left',
          label: 'Admin',
        },  
        {
          type: 'docSidebar',
          sidebarId: 'userSidebar',
          position: 'left',
          label: 'User',
        },
        {
          type: "docSidebar",
          sidebarId: "extContTestSidebar",
          position: "left",
          label: "External Content Test",
        }      
      ],
    },
    footer: {
      style: 'dark',
      copyright: `FUTR-HUB Documentation © ${new Date().getFullYear()}`,
    },
    prism: {
      theme: prismThemes.github,
      darkTheme: prismThemes.dracula,
    },
  } satisfies Preset.ThemeConfig,
  // plugins: ["@orama/plugin-docusaurus-v3"],
  themes: [
    // ... Your other themes.
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      /** @type {import("@easyops-cn/docusaurus-search-local").PluginOptions} */
      ({
        // ... Your options.
        // `hashed` is recommended as long-term-cache of index file is possible.
        hashed: true,
        docsRouteBasePath: '/',
        indexBlog: false,
        // For Docs using Chinese, The `language` is recommended to set to:
        // ```
        // language: ["en", "zh"],
        // ```
      }),
    ],
  ],
  plugins: [
    [
        "docusaurus-plugin-remote-content",
        {
            // options here
            name: "API-Documentation", // used by CLI, must be path safe
            sourceBaseUrl: "https://gitlab.com/berlintxl/futr-hub/platform/core-platform/documentation_public/API-Documentation/-/raw/integrate-docusaurus", // the base url for the markdown (gets prepended to all of the documents when fetching)
            outDir: "docs/External Content Test", // the base directory to output to.
            documents: ["README.md"], // the file names to download
        },
    ],
],
};

export default config;
