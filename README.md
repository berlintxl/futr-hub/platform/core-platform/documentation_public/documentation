# FUTR HUB Documentation

The documentation for FUTR HUB is built using [Docusaurus](https://docusaurus.io/), a modern static website generator. The documentation is hosted on GitLab Pages and can be accessed at https://docs.staging.futr-hub.de/documentation. 

If you would like to contribute to the documentation, please follow these instructions: [Contribe to the Documentation](./contribute/contribute-to-documentation.md).

For running the documentation locally, please follow the instructions below.

## Local Setup

To do local development, first git clone the repository and then run the following commands:

```bash
npm install
```

Once everything is installed, you can now just run the following command to start the local development server:

```bash
npx docusaurus start
```

This will start a local development server on `http://localhost:3000/` and you can start editing the website and see the changes in real-time.