import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import HomepageFeatures from '@site/src/components/HomepageFeatures';
import Heading from '@theme/Heading';

import styles from './index.module.css';

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero', styles.heroBanner, styles.body)}>
      <div className="container">
        <Heading as="h1" className="hero__title">
          <span style={{ color: 'white' }}>{siteConfig.title}</span>
        </Heading>
        {/* <p className="hero__subtitle" style={{ color: 'white' }}>All you need to use the FUTR HUB platform.</p>
        <br></br> */}
        <p className="hero__subtitle" style={{ color: 'white' }}>Please choose your role:</p>
        <div className={styles.buttons}>
          <Link
            className={clsx(
              'button button--outline button-white button--secondary button--lg',
              styles.getStarted,
            )}
            to="/User/User">
            <span style={{ color: 'white' }}>User</span>
          </Link>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <Link
            className={clsx(
              'button button--outline button-white button--secondary button--lg',
              styles.getStarted,
            )}
            to="/Admin/Admin">
            <span style={{ color: 'white' }}>Admin</span>
          </Link>
        </div>
      </div>
    </header>
  );
}

export default function Home(): JSX.Element {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <HomepageHeader />

    </Layout>
  );
}
