# User Documentation

<br> </br>

The documentation for users is divided into sections, which correspond to the different functionalities of the FUTR HUB project. 

## Overview of user documentation:


- [Geo Documentation](/User/User/Geo/)
- [Context Management](/User/User/ContextManagement/)
- [Data Management](/User/User/Data%20Management/)
- [Identity Management](/User/User/Identity%20Management/)

