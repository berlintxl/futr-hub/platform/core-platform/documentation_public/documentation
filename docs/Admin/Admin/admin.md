# Admin 

The documentation for admins is divided into sections, which correspond to the different functionalities of the FUTR HUB project.

## Overview of admin documentation:

- [Geo Documentation](/Admin/Admin/Geo/)
- [Context Management](/Admin/Admin/ContextManagement/)
- [Data Management](/Admin/Admin/Data%20Management/)
- [Identity Management](/Admin/Admin/Identity%20Management/)