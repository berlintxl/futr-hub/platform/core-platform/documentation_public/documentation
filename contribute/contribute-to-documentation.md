# How to Contribute to the Documentation

This guide will help you get started with contributing to the FUTR HUB documentation. The following sections will describe the structure of the documentation and how to add new content.

## Documentation Structure

All documentation is stored in the folder `docs` in the root directory of the repository. The documentation is divided into documentation for `Users` and `Admins`. Each of these folders contains subfolders for different components of the FUTR HUB platform. 

```plaintext
docs
├── Admin
│   ├── ContextManagement
│   │   ├── Context Management.md
│   │   ├── frost.md
│   │   ├── quantumleap.md
│   │   └── stellio.md
│   └── Deployment
│       ├── Deployment.md
│       └── docker.md
└── User
    ├── ContextManagement
    │   ├── Context Management.md
    │   ├── frost.md
    │   ├── quantumleap.md
    │   └── stellio.md
    └── Deployment
        ├── Deployment.md
        └── docker.md
```

--> Should be modified when actual structure is known.

A file within a folder represents a page on the website. Every folder represents a section on the website. When folders and files are nested they create dropdowns in the menu on the left side.

![alt text](./img/docusaurus_section_structure.png)

## Adding New Content

Adding new content can be done by either updating existing documentation, creating new pages in existing sections, or creating new sections with new pages. Following sections will cover all three cases.

### Updating Existing Documentation

Updating existing documentation is done by changing the content of the existing files. The following steps will guide you through the process:

1. Open the website for the documentation.
2. Navigate to the page you want to change. 
3. Click on the `Edit this page` button at the bottom of the page.
   ![alt text](./img/edit_this_page.png)
4. This will send you to the file in GitLab.
5. Click on edit and choose `Edit single file`.
   ![alt text](./img/edit_single_file.png)
6. Make the changes you want to make.
7. When done create commit message where you describe the changes you made. Click on `Commit changes`.
   ![alt text](./img/commit_changes.png)
8.  The website will be updated automatically.

### Creating New Pages in Existing Sections

Creating new pages in existing sections is done by creating a new markdown file in the corresponding folder within the GitLab repository. The following steps will guide you through the process:

1. Open the website for the documentation.
2. Navigate to the section where you want to add a new page.
3. Within any page, click on the `Edit this page` button at the bottom of the page.
  ![alt text](./img/edit_this_page.png)
1. This will send you to the file in GitLab.
2. Here navigate to the folder by going one step back in the repository. This is done by clicking on the folder name in the path: 
   ![alt text](./img/nav_to_folder.png)
3. Click on the `New file` button:
   ![alt text](./img/create_file.png)
4. Give the file a name, add content, create a commit message and click on `Commit changes`:
   ![alt text](./img/commit_file.png)
5. The website will be updated automatically.

### Creating New Sections with New Pages

Creating new sections with new pages is done by creating a new folder in the corresponding folder within the GitLab repository. The following steps will guide you through the process:

1. Open the website for the documentation.
2. Navigate to the section where you want to add a new section.
3. Within any page, click on the `Edit this page` button at the bottom of the page.
  ![alt text](./img/edit_this_page.png)
4. This will send you to the file in GitLab.
5. Here navigate to the folder by going on step back in the repository. This is done by clicking on the folder name in the path: 
   ![alt text](./img/nav_to_folder.png)
6. Click on the `New directory` button:
   ![alt text](./img/create_folder.png)
7. Give the folder a name, make a commit message and click on `Create directory`:
   ![alt text](./img/commit_folder.png)
8. You are now in the created folder. 
9. Now a file for the overview of the section must be created. This file must have the same name as the folder. Click on the `New file` button:
   ![alt text](./img/create_file.png)
10. Give the file the same name as the folder, add content, create a commit message and click on `Commit changes`:
    ![alt text](./img/commit_section_file.png)
11. You can now follow the steps from the previous section [Creating New Pages in Existing Sections](#creating-new-pages-in-existing-sections) to add new pages to the section.

